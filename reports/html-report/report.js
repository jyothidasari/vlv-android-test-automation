$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("Test.feature");
formatter.feature({
  "line": 1,
  "name": "HomePage Test Scenario",
  "description": "",
  "id": "homepage-test-scenario",
  "keyword": "Feature"
});
formatter.before({
  "duration": 256443,
  "status": "passed"
});
formatter.background({
  "line": 3,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 4,
  "name": "App is launched",
  "keyword": "Given "
});
formatter.match({
  "location": "Hooks.navigateTo()"
});
formatter.result({
  "duration": 14081822044,
  "status": "passed"
});
formatter.scenario({
  "line": 6,
  "name": "Test for Configuration",
  "description": "",
  "id": "homepage-test-scenario;test-for-configuration",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 7,
  "name": "User on Home Page with title \"HABBITZZ\"",
  "keyword": "Given "
});
formatter.match({
  "arguments": [
    {
      "val": "HABBITZZ",
      "offset": 30
    }
  ],
  "location": "TestStepDef.user_on_homePage(String)"
});
formatter.result({
  "duration": 3479611,
  "status": "passed"
});
formatter.after({
  "duration": 10946836734,
  "status": "passed"
});
});