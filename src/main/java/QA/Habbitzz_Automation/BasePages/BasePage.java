package QA.Habbitzz_Automation.BasePages;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.io.IOException;
import java.security.SecureRandom;
import java.time.Duration;
import java.util.List;

/**
 * A Base for all the pages within the suite
 */
public abstract class BasePage {

	public static OS executionOS = OS.ANDROID;
	public static AppiumDriver<? extends MobileElement> driver;
	protected static Actions act;

	protected BasePage(AppiumDriver<? extends MobileElement> driver) throws IOException {
		this.driver = driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver, Duration.ofSeconds(5)), this);
	}

	public enum OS {
		ANDROID, IOS
	}

	public static enum LocatorType {
		CLASSNAME, CSS, ID, LINK, NAME, TAGNAME, XPATH;
	}

	public static By findBy(LocatorType type, String sLocator) {
		switch (type) {
		case ID:
			return By.id(sLocator);
		case CLASSNAME:
			return By.className(sLocator);
		case XPATH:
			return By.xpath(sLocator);
		case CSS:
			return By.cssSelector(sLocator);
		case LINK:
			return By.linkText(sLocator);
		case NAME:
			return By.name(sLocator);
		case TAGNAME:
			return By.tagName(sLocator);
		}
		return null;
	}

	public static void waitForVisibilityOf(By locator, String selectorName) {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 30);
			wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
		} catch (Exception e) {
			System.out.println("ERROR: Element " + selectorName + " not Visibile");
		}
	}

	public static void waitForPresenceOf(By locator, String selectorName) {

		try {
			WebDriverWait wait = new WebDriverWait(driver, 30);
			wait.until(ExpectedConditions.presenceOfElementLocated(locator));
		} catch (Exception e) {
			System.out.println("ERROR: Element " + selectorName + " not Presence");
		}
	}

	public static void waitForClickabilityOf(By locator, String selectorName) {

		try {
			WebDriverWait wait = new WebDriverWait(driver, 30);
			wait.until(ExpectedConditions.elementToBeClickable(locator));
		} catch (Exception e) {
			System.out.println("ERROR: Element " + selectorName + " not Clickable");
		}
	}

	public static MobileElement findObject(By ele, String selectorName) {
		MobileElement rClientElement = null;
		try {
			waitForPresenceOf(ele, selectorName);
			rClientElement = driver.findElement(ele);
		} catch (Exception e) {
			System.out.println("ERROR: Element " + selectorName + "  not found");
		}
		return rClientElement;
	}

	public static List<? extends MobileElement> findObjects(By ele, String selectorName) {
		List<? extends MobileElement> rClientElement = null;
		try {
			waitForPresenceOf(ele, selectorName);
			rClientElement = driver.findElements(ele);
		} catch (Exception e) {
			System.out.println("ERROR: Element " + selectorName + "  not found");
		}
		return rClientElement;
	}

	public static void clickElement(MobileElement ele, String selector) {
		try {
			ele.click();
			System.out.println("INFO: Clicking on " + selector);
		} catch (Exception e) {
			System.out.println("ERROR: Element " + selector + " is unclickable ");
		}
	}

	public static void clickElement(List<MobileElement> ele, String selector, int index) {
		try {
			ele.get(index).click();
			System.out.println("INFO: Clicking on " + selector);
		} catch (Exception e) {
			System.out.println("ERROR: Element " + selector + " is unclickable ");
		}

	}

	public static void submit(MobileElement ele, String selector) {
		try {
			ele.submit();
			System.out.println("INFO: Clicking on " + selector);
		} catch (Exception e) {
			System.out.println("ERROR: Element " + selector + " is unclickable ");
		}
	}

	public static void doubleclickElement(By ele, String selector) {
		MobileElement wEle = findObject(ele, selector);
		try {
			act.doubleClick(wEle).build().perform();
			System.out.println("INFO: Double clicking on " + selector);
		} catch (Exception e) {
			System.out.println("ERROR: Element " + selector + " is unclickable ");
		}

	}

	public static void doubleclickElementFromList(By ele, String selector, int index) {
		List<? extends MobileElement> wEle = findObjects(ele, selector);
		try {
			act.doubleClick(wEle.get(index)).build().perform();
			System.out.println("INFO: Double clicking on " + selector);
		} catch (Exception e) {
			System.out.println("ERROR: Element " + selector + " is unclickable ");
		}

	}

	public static void setText(MobileElement ele, String selector, String sText) {
		ele.sendKeys(sText);
		System.out.println("INFO: Entering " + sText + " in " + selector);
		if (executionOS.equals(OS.ANDROID))
			driver.hideKeyboard();
	}

	public static void setText(By ele, String selector, String... sText) {
		List<? extends MobileElement> wEle = findObjects(ele, selector);
		for (int i = 0; i < sText.length; i++) {
			wEle.get(i).sendKeys(sText[i]);
			System.out.println("INFO: Entering " + sText[i] + " in " + selector);
			// ((AndroidDriver)driver).pressKeyCode(66);
			driver.hideKeyboard();

		}
	}

	public static void setValue(MobileElement ele, String selector, String sText) {
		ele.setValue(sText);
		System.out.println("INFO: Entering " + sText + " in " + selector);
		if (executionOS.equals(OS.ANDROID))
			driver.hideKeyboard();
	}

	public static String getText(MobileElement ele, String selector) {
		String text = ele.getText();
		System.out.println("INFO: Geting text from " + selector + " is '" + text + "'");
		return text;
	}

	public static void clearText(MobileElement ele, String selector) {
		ele.clear();
	}

	public static void setSelectBoxText(By ele, String selectorName, String sText) {
		WebElement wEle = findObject(ele, selectorName);
		Select sel = new Select(wEle);
		sel.selectByVisibleText(sText);
		System.out.println("INFO: Set Text" + selectorName);
	}

	public static boolean isElementPresent(WebElement ele, String selector) {
		boolean status = false;
		try {
			status = ele.isDisplayed();
			return status;
		} catch (Exception e) {
			System.out.println("ERROR: Element " + selector + " is not present on screen ");
		}
		return status;
	}

	public static boolean isElementPresent(List<MobileElement> ele, String selector, int index) {
		boolean status = false;
		try {
			status = ele.get(index).isDisplayed();
			return status;
		} catch (Exception e) {
			System.out.println("ERROR: Element " + selector + " is not present on screen ");
		}
		return status;
	}

	public static boolean isElementEnabled(MobileElement ele, String selector) {
		boolean status = false;
		try {
			status = ele.isEnabled();
			return status;
		} catch (Exception e) {
			System.out.println("ERROR: Element " + selector + " is not Enabled on screen ");
		}
		return status;
	}

	public static boolean isElementEnabled(List<MobileElement> ele, String selector, int index) {
		boolean status = false;
		try {
			status = ele.get(index).isEnabled();
			return status;
		} catch (Exception e) {
			System.out.println("ERROR: Element " + selector + " is not Enabled on screen ");
		}
		return status;
	}

	public static boolean isElementSelected(By ele, String selector) {
		boolean status = false;
		MobileElement wEle = findObject(ele, selector);
		try {
			status = wEle.isSelected();
			return status;
		} catch (Exception e) {
			System.out.println("ERROR: Element " + selector + " is not Selected on screen ");
		}
		return status;
	}

	public static String getRandomString(int length) {
		final String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
		SecureRandom rnd = new SecureRandom();
		StringBuilder sb = new StringBuilder(length);
		for (int i = 0; i < length; i++)
			sb.append(AB.charAt(rnd.nextInt(AB.length())));
		return sb.toString();

	}

	public static String getRandomStringsmallchr(int length) {
		final String AB = "abcdefghijklmnopqrstuvwxyz";
		SecureRandom rnd = new SecureRandom();
		StringBuilder sb = new StringBuilder(length);
		for (int i = 0; i < length; i++)
			sb.append(AB.charAt(rnd.nextInt(AB.length())));
		return sb.toString();

	}

	public static String getRandomIntiger(int length) {
		final String AB = "0123456789";
		SecureRandom rnd = new SecureRandom();
		StringBuilder sb = new StringBuilder(length);
		for (int i = 0; i < length; i++)
			sb.append(AB.charAt(rnd.nextInt(AB.length())));
		return sb.toString();

	}

	public static String getNewRandomUser() {
		String user = "TestUser";
		user = user.concat(getRandomString(3));
		return user.concat("@ethos.com");
	}

	public static void swipeHorizontal(MobileElement ele, String selectorName) throws Exception {
		Dimension size = driver.manage().window().getSize();
		// System.out.println("width>>"+size.width+" height>>"+size.height);
		int anchor = (int) (size.height - 30);
		int startPoint = (int) (size.width * 0.80);
		int endPoint = (int) (size.width * 0.10);
		// System.out.println("anchor>>"+anchor+" startPoint>>"+startPoint+"
		// endPoint>>>>"+endPoint);
		new TouchAction(driver).press(PointOption.point(startPoint, anchor))
				.waitAction(WaitOptions.waitOptions(Duration.ofSeconds(2))).moveTo(PointOption.point(endPoint, anchor))
				.release().perform();
		System.out.println("INFO: Scrolling Right to Left to find Element " + selectorName);
	}

	public static void quitAppiumSession() {
		if (driver != null) {
			driver.quit();
		}
	}

	public static void restartApp() throws InterruptedException {
		driver.resetApp();
		Thread.sleep(10000);
	}

}