package QA.Habbitzz_Automation.BasePages;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import java.io.IOException;
import java.net.URL;
import java.time.Duration;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.support.PageFactory;
import QA.Habbitzz_Automation.BasePages.BasePage;

public class AppiumController extends TestBase {
	
	public AppiumController() {
		PageFactory.initElements(new AppiumFieldDecorator(driver, Duration.ofSeconds(5)), this);
	}

	@Override
	protected void setAppiumDriver(String url) throws IOException {
		if (driver != null) {
			return;
		}
		
		switch (BasePage.executionOS) {
		case ANDROID:
			System.out.println("INFO: Setting up AndroidDriver");
			final String URL_STRING = "http://127.0.0.1:4723/wd/hub";
			URL serverUrl = new URL(URL_STRING);
			driver = new AndroidDriver<MobileElement>(serverUrl, capabilities);
			System.out.println("INFO: Initial Android Setup Done");
			try {
				Thread.sleep(10000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
			break;
		case IOS:
		
			break;
		}
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

	}

	@Override
	protected String getDesiredCapabilitiesPropertiesFileName() {
		switch (BasePage.executionOS) {
		case ANDROID:
			return "desiredCapabilities.android.properties";
		case IOS:
			return "desiredCapabilities.ios.properties";
		default:
			return "";
		}
	}


}
