package QA.Habbitzz_Automation.BasePages;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import QA.Habbitzz_Automation.Utils.EnvironmentUtils;
import java.io.IOException;
import java.util.Properties;
import java.util.Set;

/**
 * An abstract base for all of the Android tests within this package
 * Responsible for setting up the Appium test Driver
 */

public abstract class TestBase {
	/**
	 * Make the driver static. This allows it to be created only once and used
	 * across all of the test classes.
	 */
	public static AppiumDriver<? extends MobileElement> driver;
	public static String path = System.getProperty("user.dir");
	public static Actions act;
	public static DesiredCapabilities capabilities;
	private static final String LOCAL_APPIUM_ADDRESS = "http://127.0.1.1:4723";
	public static Properties properties;
	//public abstract String getName();
	

	//public abstract void setUpPage();

	protected abstract void setAppiumDriver(String url) throws IOException;

	protected abstract String getDesiredCapabilitiesPropertiesFileName();

	/**
	 * This method does take and set Capabilities from propertie file or System
	 * propertie for Appium
	 */

	public void setUpAppiumDriver(String deviceName_) throws IOException {
		DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
		desiredCapabilities = getDesiredCapabilitiesFromProperties(deviceName_);
		capabilities = desiredCapabilities;
		System.out.println("INFO: Setting client side specific capabilities...");
		//String fileUUID = getDefaultFileUUID();
		//capabilities.setCapability("app", path + "/" + fileUUID);
		System.out.println("INFO: Setting client side specific capabilities... FINISHED");
		System.out.println("INFO: Creating Appium session, this may take couple minutes.."
				+ properties.getProperty(deviceName_ + "URL"));
		setAppiumDriver(properties.getProperty(deviceName_ + "URL"));
	}

	private DesiredCapabilities getDesiredCapabilitiesFromProperties(String deviceName_) {
		System.out
				.println("INFO: Setting desiredCapabilities defined in " + getDesiredCapabilitiesPropertiesFileName());
		DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
		properties = EnvironmentUtils.fetchProperties(getDesiredCapabilitiesPropertiesFileName());
		Set<String> keys = properties.stringPropertyNames();
		for (String key : keys) {

			if (key.contains(deviceName_) && !key.contains("URL")) {
				String value = properties.getProperty(key);
				desiredCapabilities.setCapability(key.substring(deviceName_.length()), value);
			}
		}
		return desiredCapabilities;
	}
	
	public String getAppiumServerAddress() {
		return LOCAL_APPIUM_ADDRESS;
	}
	
	public static AppiumDriver<? extends MobileElement> getDriver() {
		return driver;
	} 
	
}