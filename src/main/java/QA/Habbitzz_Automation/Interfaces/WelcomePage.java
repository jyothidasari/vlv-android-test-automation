package QA.Habbitzz_Automation.Interfaces;

/**
 * A Registration Page
 */
public interface WelcomePage {

	//////////////////////////////////////////////////////////
	////////////////////// Clickers //////////////////////////
	//////////////////////////////////////////////////////////

	public void clickOnEnglish() ;
	
	public  void clickOnEnglishButton() ;

	public  void clickOnShoppingButton() throws InterruptedException ;

	//////////////////////////////////////////////////////////
	////////////////////// Other //////////////////////////
	//////////////////////////////////////////////////////////

	public  void swipeWelcomePages() throws Exception;
}
