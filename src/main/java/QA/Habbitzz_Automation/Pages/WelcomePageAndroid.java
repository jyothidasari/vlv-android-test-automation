package QA.Habbitzz_Automation.Pages;

import java.io.IOException;
import org.openqa.selenium.By;
import QA.Habbitzz_Automation.BasePages.BasePage;
import QA.Habbitzz_Automation.Interfaces.WelcomePage;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;

public class WelcomePageAndroid extends BasePage implements WelcomePage {

	
	public WelcomePageAndroid(AppiumDriver driver) throws IOException {
		super(driver);
	}

	public static String englishButton = "com.habbitzz.app:id/btn_english";
	public static String shoppingButton = "com.habbitzz.app:id/btn_start_shopping";
	
	@AndroidFindBy(id = "com.habbitzz.app:id/btn_english")
	private MobileElement btn_Login;
	
	//////////////////////////////////////////////////////////
	////////////////////// Clickers //////////////////////////
	//////////////////////////////////////////////////////////

	public void clickOnEnglish() {
		//WebElement ele= findObject( findBy(LocatorType.ID, englishButton), "English Button") ;
		//	clickElement(btn_Login, "English Button");
		btn_Login.click();
		}
	
	public void clickOnEnglishButton() {
	MobileElement ele= findObject( findBy(LocatorType.ID, englishButton), "English Button") ;
		clickElement(ele, "English Button");
	}

	public void clickOnShoppingButton() throws InterruptedException {
		MobileElement ele=findObject(findBy(LocatorType.ID, shoppingButton), "Shopping Button") ;
		clickElement(ele, "Shopping Button");
	
	}

	//////////////////////////////////////////////////////////
	////////////////////// Other //////////////////////////
	//////////////////////////////////////////////////////////

	public void swipeWelcomePages() throws Exception {
		for (int i = 0; i <= 2; i++) {
			BasePage.swipeHorizontal(driver.findElement(By.id("com.habbitzz.app:id/tv_title")), "Welcome Page");
		}
	}

}
