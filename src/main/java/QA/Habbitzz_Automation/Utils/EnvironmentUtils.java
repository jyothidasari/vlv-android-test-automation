package QA.Habbitzz_Automation.Utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import org.apache.commons.configuration.ConfigurationException;
import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;

public class EnvironmentUtils {

	private final String propertyKey;
	final String decryptedUserPassword;
	public static Properties properties;

	public EnvironmentUtils(String pUserPasswordKey) throws Exception {
		this.propertyKey = pUserPasswordKey;
		decryptedUserPassword = decryptPropertyValue();
	}

	public static Properties fetchProperties(String filename) {
		properties = new Properties();
		InputStream input = null;
		try {
			input = EnvironmentUtils.class.getClassLoader().getResourceAsStream(filename);
			properties.load(input);

		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return properties;
	}

	private String decryptPropertyValue() throws ConfigurationException {
		String encryptedPropertyValue = properties.getProperty(propertyKey);
		StandardPBEStringEncryptor encryptor = new StandardPBEStringEncryptor();
		encryptor.setPassword("jasypt");
		String decryptedPropertyValue = encryptor.decrypt(encryptedPropertyValue);
		return decryptedPropertyValue;
	}

}
