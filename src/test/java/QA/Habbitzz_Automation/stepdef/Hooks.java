package QA.Habbitzz_Automation.stepdef;

import QA.Habbitzz_Automation.BasePages.BasePage;
import QA.Habbitzz_Automation.BasePages.TestBase;
import QA.Habbitzz_Automation.Interfaces.WelcomePage;
import QA.Habbitzz_Automation.Pages.WelcomePageAndroid;
import cucumber.api.java.After;
import cucumber.api.java.en.Given;

public class Hooks {
	protected WelcomePage welcomePage;
	
	@Given("^App is launched$")
	public void navigateTo() throws Exception {
		System.out.println("*************  STARTING NEW TEST  **************");
		System.out.println("INFO: Initial Setup for New Android Test");
		switch (BasePage.executionOS) {
		case ANDROID:
			welcomePage=new WelcomePageAndroid(TestBase.getDriver());
			break;
		case IOS:

			break;
		}
		welcomePage.clickOnEnglishButton();
		welcomePage.swipeWelcomePages();
		welcomePage.clickOnShoppingButton();
	}

	@After
	public void restartApp() throws InterruptedException {
		TestBase.getDriver().resetApp();
	}

}
