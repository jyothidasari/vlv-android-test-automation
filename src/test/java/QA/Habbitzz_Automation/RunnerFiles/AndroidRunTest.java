package QA.Habbitzz_Automation.RunnerFiles;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import QA.Habbitzz_Automation.BasePages.AppiumController;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(tags = {}, features = {
"src/test/resources/features/"}, glue = {
		"QA.Habbitzz_Automation.stepdef" }, plugin = {
				"pretty", "html:reports/html-report",
				"json:target/cucumberReport.json" }, monochrome = true, strict = true)
public class AndroidRunTest  {
	
	public static AppiumController appiumController=new AppiumController();
	
	@BeforeClass
	public static void setUp() throws Exception {
		appiumController.setUpAppiumDriver("FirstDevice");

	}

	@AfterClass
	public static void tearDown() throws InterruptedException {
		appiumController.getDriver().quit();
	}

}

